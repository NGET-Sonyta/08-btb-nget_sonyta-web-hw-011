import React from 'react';
import { Container, Button, Row, Col, Form} from 'react-bootstrap';
import './stylesheet.css';
import 'bootstrap/dist/css/bootstrap.css';

export default class StateProps extends React.Component {  
    constructor(props){
        super(props);

        this.state = {myResult : []};
        
        this.handleClick = this.handleClick.bind(this);
        
      }
    
    handleClick(e) {
        e.preventDefault();

        var result = 0;
        var valA = this.refs.myA.value;
        var valB= this.refs.myB.value;
        var selected=document.getElementById('mySelect').value;
        var numbers = /^[0-9]+$/;
        //console.log("valA : " + valA + "_____ valB : " + valB);
            if(valA.match(numbers) && valB.match(numbers))
            {   
                if ((selected==='1')){
                     result= Number(valA)+ Number(valB);
                }else if (selected==='2'){
                    if(valB> valA){
                        alert("Cannot Substract Bigger Number.");
                    }else
                     result= (valA-valB);
                }else if (selected==='3'){
                     result= (valA*valB);
                }else if (selected==='4'){
                     result= (valA/valB);
                }else if (selected==='5'){
                     result= (valA%valB);
                } 
                var myArr = this.state.myResult;
                myArr.push(result);
                this.setState({
                    myResult : myArr
                });   
                document.getElementById("A").value="";
                document.getElementById("B").value="";
            return true;
            }
            else
            {
            alert('Only NUMBERS are allowed in Textbox.');
            return false;
            }
           
     }

    componentDidUpdate(){}
   
    render() {
        return (
            <div style={{marginLeft:'10%'}}>
                <div>
                   
                    <Container>
                      <Row>
                        <Col md={6} sm={10}  style={{border:'solid', backgroundColor:'aliceblue', marginTop:'20px'}}>
                            <img src="/calculator.png" alt="Calculator"/>
                            <div className='big-input'>
                            
                            <div className='input'>
                                <input id="A" ref="myA" style={{width:'350px',height:'50px'}} type="text"/>
                            </div>
                            <div className='input'>
                                <input id="B" ref="myB" style={{width:'350px',height:'50px'}} type="text"/>
                            </div>
                            <Form.Control id='mySelect' as="select" style={{width:'350px',height:'50px', marginTop:'10px'}}>
                                    <option value='1'>Plus (+)</option>
                                    <option value='2'>Substract (-)</option>
                                    <option value='3'>Multiply (x)</option>
                                    <option value='4'>Divide (/)</option>
                                    <option value='5'>Module (%)</option>
                                    </Form.Control>
                                <Button variant="primary" style={{marginTop:'15px',height: '50px',width: '200px', marginLeft:'23%', color:'white', marginBottom:'40px'}}
                                onClick={this.handleClick}>
                                    Calculate
                                </Button>
                                

                            </div>
                        </Col>
                        <Col md={4} sm={8} style={{marginLeft:'5%', marginTop:'20px'}}> 
                            <div className='Title'>
                                <h3>Result History: </h3>
                                 {this.state.myResult.map((item,index)=>{
                                    return (
                                         <div key={index}>
                                             {item}
                                         </div>

                                    )
                                }
                                )}
                               
                            </div>
                        </Col>
                      </Row>
                    </Container>
            
                </div>
            </div>
            
        )
    }
}
